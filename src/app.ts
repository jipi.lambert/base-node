import express from 'express';

class App {
    public express

    constructor(){
            this.express = express()
            this.initRoutes()
        }


    private initRoutes():void{
        const router = express.Router()
        router.get('/', (req,res)=>{
            res.json({
                message: "Salllut!"
            })
        })
        this.express.use('/', router)
    }

}

export default new App().express